package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 25-06-2019.
 */

public interface ForgetPasswordPresenter extends BasePresenter {
    void submitEmail(String email);
    interface View{
        void showLoader();
        void hideLoader();
        void onSuccess();
    }
}
