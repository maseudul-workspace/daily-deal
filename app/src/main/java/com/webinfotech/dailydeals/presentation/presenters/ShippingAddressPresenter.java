package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void setAddressAdapter(ShippingAddressAdapter addressAdapter);
        void goToEditAddressActivity(int position);
        void showLoader();
        void hideLoader();
    }
}
