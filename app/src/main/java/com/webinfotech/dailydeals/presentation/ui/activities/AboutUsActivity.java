package com.webinfotech.dailydeals.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.webinfotech.dailydeals.AndroidApplication;
import com.webinfotech.dailydeals.R;
import com.webinfotech.dailydeals.domain.models.SettingData;

public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_about_us)
    TextView txtViewAboutUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        getSupportActionBar().setTitle("About Us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        SettingData settingData = androidApplication.getSettingData(this);
        txtViewAboutUs.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(settingData.about, HtmlCompat.FROM_HTML_MODE_COMPACT)), HtmlCompat.FROM_HTML_MODE_COMPACT));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
