package com.webinfotech.dailydeals.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.dailydeals.AndroidApplication;
import com.webinfotech.dailydeals.R;
import com.webinfotech.dailydeals.domain.executors.impl.ThreadExecutor;
import com.webinfotech.dailydeals.domain.models.ChargesWrapper;
import com.webinfotech.dailydeals.domain.models.User.UserInfo;
import com.webinfotech.dailydeals.presentation.presenters.CartDetailsActivityPresenter;
import com.webinfotech.dailydeals.presentation.presenters.impl.CartDetailsActivityPresenterImpl;
import com.webinfotech.dailydeals.presentation.ui.adapters.CartAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.dailydeals.presentation.ui.dialogs.DeliverySpeedDialog;
import com.webinfotech.dailydeals.threading.MainThreadImpl;

public class CartListActivity extends AppCompatActivity implements CartDetailsActivityPresenter.View, DeliverySpeedDialog.Callback {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    CartDetailsActivityPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.layout_summary)
    View layoutSummary;
    @BindView(R.id.txt_view_sub_total)
    TextView txtViewSubTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    @BindView(R.id.txt_view_grand_total)
    TextView txtViewGrandTotal;
    @BindView(R.id.btn_delivery_address)
    Button btnDeliveryAddress;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    @BindView(R.id.wallet_check_box)
    CheckBox walletCheckbox;
    double grandTotal;
    double walletAmount;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;
    @BindView(R.id.layout_wallet_pay)
    View walletPay;
    ChargesWrapper chargesWrapper;
    DeliverySpeedDialog deliverySpeedDialog;
    @BindView(R.id.layout_delivery_charges)
    View layoutDeliveryCharges;
    @BindView(R.id.txt_view_order_warning)
    TextView txtViewWarning;
    @BindView(R.id.txt_view_delivery_charges)
    TextView txtViewDeliveryCharges;
    int deliveryType;
    @BindView(R.id.coupon_code_layout)
    View couponCodeLayout;
    @BindView(R.id.layout_coupon_discount)
    View layoutCouponDiscount;
    @BindView(R.id.txt_view_coupon_discount)
    TextView txtViewCouponDiscount;
    double couponDiscount = 0;
    @BindView(R.id.edit_text_coupon_code)
    EditText editTextCouponCode;
    String couponCode;
    int deliveryStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAddressBottomSheetDialog();
        setWalletCheckboxListener();
        initialiseDeliverySpeedDialog();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.getCharges();
        mPresenter.fetchWalletAmount();
    }

    private void initialisePresenter() {
        mPresenter = new CartDetailsActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDeliverySpeedDialog() {
        deliverySpeedDialog = new DeliverySpeedDialog(this, this, this);
        deliverySpeedDialog.setUpDialogView();
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    private void setWalletCheckboxListener() {
        walletCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (grandTotal >= walletAmount) {
                        double net = grandTotal - walletAmount;
                        txtViewGrandTotal.setText("₹ " + net);
                        walletCheckbox.setText("Wallet Pay(₹ 0.0)");
                        txtViewWalletAmount.setText("₹ " + walletAmount);
                    } else {
                        txtViewGrandTotal.setText("₹ 0.0");
                        walletCheckbox.setText("Wallet Pay(₹ " + (walletAmount - grandTotal) + ")");
                        txtViewWalletAmount.setText("₹ " + grandTotal);
                    }
                    mPresenter.setWalletPayStatus(2);
                    walletPay.setVisibility(View.VISIBLE);
                } else {
                    txtViewGrandTotal.setText("₹ " + grandTotal);
                    mPresenter.setWalletPayStatus(1);
                    walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ")");
                    walletPay.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void loadCharges(ChargesWrapper chargesWrapper) {
        this.chargesWrapper = chargesWrapper;
        this.txtViewDeliveryCharges.setText("Rs. " + chargesWrapper.minPurchaseCharge);
        deliverySpeedDialog.setDeliveryExpress(chargesWrapper.expressCharge);
        txtViewWarning.setText("*Orders below Rs " +  chargesWrapper.minPurchaseAmount + " will have delivery charges of Rs " + chargesWrapper.minPurchaseCharge);
        mPresenter.getCartList();
    }

    @Override
    public void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal) {

        this.grandTotal = grandTotal;

        if (grandTotal < this.chargesWrapper.minPurchaseAmount) {
            layoutDeliveryCharges.setVisibility(View.VISIBLE);
            this.grandTotal = this.grandTotal + this.chargesWrapper.minPurchaseCharge;
        } else {
            layoutDeliveryCharges.setVisibility(View.GONE);
        }

        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCartList.setAdapter(adapter);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewCartList.addItemDecoration(itemDecor);

        if (walletCheckbox.isChecked()) {
            if (this.grandTotal > walletAmount) {
                double net = this.grandTotal - walletAmount;
                txtViewGrandTotal.setText("₹ " + net);
            } else {
                txtViewGrandTotal.setText( "₹ 0.0");
            }
        } else {
            txtViewGrandTotal.setText("₹ " + this.grandTotal);
        }

        if (this.couponDiscount >= 1) {
            this.grandTotal = this.grandTotal - couponDiscount;
            txtViewGrandTotal.setText("₹ " + this.grandTotal);
        }

        layoutSummary.setVisibility(View.VISIBLE);
        txtViewSubTotal.setText("₹ " + subTotal);
        txtViewDiscount.setText("₹ " + discount);
        btnDeliveryAddress.setVisibility(View.VISIBLE);
        txtViewWarning.setVisibility(View.VISIBLE);
        couponCodeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void loadWalletAmount(double walletAmount) {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo.isStar == 1) {
            walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ") You must be star member to use wallet");
            walletCheckbox.setEnabled(false);
        } else {
            walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ")");
        }
        this.walletAmount = walletAmount;
        if (walletAmount < 1) {
            walletCheckbox.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadCoupon(double coupon) {
        if (coupon < 1) {
            Toasty.warning(this, "Invalid Coupon Code").show();
        } else {
            layoutCouponDiscount.setVisibility(View.VISIBLE);
            txtViewCouponDiscount.setText("₹ " + coupon);
            this.grandTotal = this.grandTotal - coupon;
            txtViewGrandTotal.setText("₹ " + this.grandTotal);
            couponCode = editTextCouponCode.getText().toString();
            this.couponDiscount = coupon;
        }
    }

    @Override
    public void onDeliverButtonClicked() {
        deliverySpeedDialog.showDialog();
    }

    @Override
    public void goToOrderHistory() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void hideViews() {
        recyclerViewCartList.setVisibility(View.GONE);
        layoutSummary.setVisibility(View.GONE);
        btnDeliveryAddress.setVisibility(View.GONE);
        walletCheckbox.setVisibility(View.GONE);
        couponCodeLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingProgress() {
        progressDialog.show();
    }

    @Override
    public void hideLoadingProgress() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_delivery_address) void onDeliveryAddressClicked() {
        addressBottomSheetDialog.show();
    }

    @OnClick(R.id.btn_coupon_submit) void onCouponCodeSubmitted() {
        if (editTextCouponCode.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Insert Coupon Code").show();
        } else {
            mPresenter.fetchCoupon(editTextCouponCode.getText().toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onProccedClicked() {
        mPresenter.placeOrder(deliveryStatus, couponCode);
    }

    @Override
    public void onDeliveryOptionsSelected(int id) {
        deliveryStatus = id;
    }
}
