package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.dailydeals.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductListVerticalAdapter;

public interface MainPresenter {
    void getCategories();
    void getMainData();
    void fetchCartCount();
    void fetchStarProductsCount();
    interface View {
        void loadCategoriesAdapter(MainCategoriesAdapter adapter);
        void goToSubcategoryActivity(int catId, String categoryName);
        void goToProductList(int categoryId, String categoryName);
        void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductListVerticalAdapter bestSellingProductsAdapter, ProductListVerticalAdapter popularProductsAdapter, ImageSlider[] imageSliders);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void loadCartCount(int count);
        void loadStarProductsCount(int count);
        void showLoginSnackbar();
        void showGoToCartSnackbar();
    }
}
