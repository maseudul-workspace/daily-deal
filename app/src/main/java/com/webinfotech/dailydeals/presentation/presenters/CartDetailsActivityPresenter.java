package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.domain.models.ChargesWrapper;
import com.webinfotech.dailydeals.presentation.presenters.base.BasePresenter;
import com.webinfotech.dailydeals.presentation.ui.adapters.CartAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.CartShippingAddressAdapter;

/**
 * Created by Raj on 10-01-2019.
 */

public interface CartDetailsActivityPresenter extends BasePresenter {
    void getCharges();
    void getCartList();
    void fetchShippingAddress();
    void fetchWalletAmount();
    void fetchCoupon(String coupon);
    void setWalletPayStatus(int walletPayStatus);
    void placeOrder(int deliveryStatus, String couponCode);
    interface View{
        void loadCharges(ChargesWrapper chargesWrapper);
        void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal);
        void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void loadWalletAmount(double walletAmount);
        void loadCoupon(double coupon);
        void onDeliverButtonClicked();
        void goToOrderHistory();
        void hideViews();
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
