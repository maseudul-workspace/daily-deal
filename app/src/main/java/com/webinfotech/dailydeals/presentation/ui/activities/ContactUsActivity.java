package com.webinfotech.dailydeals.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.webinfotech.dailydeals.AndroidApplication;
import com.webinfotech.dailydeals.R;
import com.webinfotech.dailydeals.domain.models.SettingData;

public class ContactUsActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_contact_us)
    TextView txtViewContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        getSupportActionBar().setTitle("Contact Us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        SettingData settingData = androidApplication.getSettingData(this);
        txtViewContactUs.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(settingData.contactUs, HtmlCompat.FROM_HTML_MODE_COMPACT)), HtmlCompat.FROM_HTML_MODE_COMPACT));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
