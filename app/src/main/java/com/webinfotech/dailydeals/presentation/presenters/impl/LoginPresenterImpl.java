package com.webinfotech.dailydeals.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.dailydeals.AndroidApplication;
import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.CheckLoginInteractor;
import com.webinfotech.dailydeals.domain.interactors.impl.CheckLoginInteractorImpl;
import com.webinfotech.dailydeals.domain.models.User.UserInfo;
import com.webinfotech.dailydeals.presentation.presenters.LoginPresenter;
import com.webinfotech.dailydeals.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.dailydeals.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteractorImpl checkLoginInteractor;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, email, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        mView.onLoginSuccess();
        Toasty.success(mContext, "Login Successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
