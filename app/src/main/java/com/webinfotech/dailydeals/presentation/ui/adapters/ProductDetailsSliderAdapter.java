package com.webinfotech.dailydeals.presentation.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.dailydeals.R;
import com.webinfotech.dailydeals.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.dailydeals.domain.models.Testing.HomeSliders;
import com.webinfotech.dailydeals.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ProductDetailsSliderAdapter extends PagerAdapter {

    Context mContext;
    ImageSlider[] images;

    public ProductDetailsSliderAdapter(Context mContext, ImageSlider[] images) {
        this.mContext = mContext;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/uploads/product_image/" + images[position].image);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
