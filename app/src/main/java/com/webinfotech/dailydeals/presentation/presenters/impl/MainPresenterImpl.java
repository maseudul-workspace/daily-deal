package com.webinfotech.dailydeals.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.dailydeals.AndroidApplication;
import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.AddToCartInteractor;
import com.webinfotech.dailydeals.domain.interactors.FetchStarProductsCountInteractor;
import com.webinfotech.dailydeals.domain.interactors.GetCartDetailsInteractor;
import com.webinfotech.dailydeals.domain.interactors.GetCategoryListInteractor;
import com.webinfotech.dailydeals.domain.interactors.GetMainDataInteractor;
import com.webinfotech.dailydeals.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.dailydeals.domain.interactors.impl.FetchStarProductsCountInteractorImpl;
import com.webinfotech.dailydeals.domain.interactors.impl.GetCartDetailsInteractorImpl;
import com.webinfotech.dailydeals.domain.interactors.impl.GetCategoryListInteractorImpl;
import com.webinfotech.dailydeals.domain.interactors.impl.GetMainDataInteractorImpl;
import com.webinfotech.dailydeals.domain.models.Cart.Cart;
import com.webinfotech.dailydeals.domain.models.Category.Category;
import com.webinfotech.dailydeals.domain.models.MainData;
import com.webinfotech.dailydeals.domain.models.User.UserInfo;
import com.webinfotech.dailydeals.presentation.presenters.MainPresenter;
import com.webinfotech.dailydeals.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.dailydeals.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.dailydeals.repository.Category.CategoryRepositoryImpl;
import com.webinfotech.dailydeals.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.dailydeals.repository.ProductRepository.impl.GetProductsRepositoryImpl;
import com.webinfotech.dailydeals.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    GetCategoryListInteractor.Callback,
                                                                    MainCategoriesAdapter.Callback,
                                                                    GetMainDataInteractor.Callback,
                                                                    ProductHorizontalAdapter.Callback,
                                                                    GetCartDetailsInteractor.Callback,
                                                                    FetchStarProductsCountInteractor.Callback,
                                                                    ProductListVerticalAdapter.Callback,
                                                                    AddToCartInteractor.Callback

{

    Context mContext;
    MainPresenter.View mView;
    GetCategoryListInteractorImpl getCategoryListInteractor;
    GetMainDataInteractorImpl getMainDataInteractor;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    FetchStarProductsCountInteractorImpl fetchStarProductsCountInteractor;
    AddToCartInteractorImpl addToCartInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getCategories() {
        getCategoryListInteractor = new GetCategoryListInteractorImpl(mExecutor, mMainThread, new CategoryRepositoryImpl(), this);
        getCategoryListInteractor.execute();
    }

    @Override
    public void getMainData() {
        getMainDataInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this);
        getMainDataInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    userInfo.userId,
                    userInfo.apiKey);
            getCartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchStarProductsCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchStarProductsCountInteractor = new FetchStarProductsCountInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchStarProductsCountInteractor.execute();
        }
    }

    @Override
    public void onGettingCategoryListSuccess(Category[] categories) {
        MainCategoriesAdapter mainCategoriesAdapter = new MainCategoriesAdapter(mContext, categories, this);
        mView.loadCategoriesAdapter(mainCategoriesAdapter);
    }

    @Override
    public void onGettingCategoryFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(int catId, String categoryName, boolean subcategoryStatus) {
        if (subcategoryStatus) {
            mView.goToSubcategoryActivity(catId, categoryName);
        } else {
            mView.goToProductList(catId, categoryName);
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        try {
            ProductHorizontalAdapter newProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.newArrivals, this);
            ProductListVerticalAdapter bestSellingProductsAdapter = new ProductListVerticalAdapter(mContext, mainData.bestSellingProducts, this);
            ProductListVerticalAdapter popularProductsAdapter = new ProductListVerticalAdapter(mContext, mainData.popularProducts, this);
            mView.loadMainData(newProductsAdapter, bestSellingProductsAdapter, popularProductsAdapter, mainData.imageSliders);
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setSettingData(mContext, mainData.settingData);
        } catch (NullPointerException e) {

        }

    }

    @Override
    public void onGettingMainDataFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartClicked(int productId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, userInfo.apiKey, productId, userInfo.userId, 1);
            addToCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showLoginSnackbar();
        }
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {
        mView.loadCartCount(0);
    }

    @Override
    public void onGettingStarProductsCountSuccess(int starProductsCount) {
        mView.loadStarProductsCount(starProductsCount);
    }

    @Override
    public void onGettingStarProductsCountFail(String errorMsg) {
        mView.loadStarProductsCount(0);
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideLoader();
        fetchCartCount();
        mView.showGoToCartSnackbar();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
