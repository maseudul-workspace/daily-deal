package com.webinfotech.dailydeals.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.webinfotech.dailydeals.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class DeliverySpeedDialog {

    public interface Callback {
        void onProccedClicked();
        void onDeliveryOptionsSelected(int id);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.radio_group_delivery)
    RadioGroup radioGroupDelivery;
    @BindView(R.id.radio_btn_express)
    RadioButton radioBtnExpress;
    @BindView(R.id.radio_btn_next_day)
    RadioButton radioButtonNextDay;
    boolean isSelected = false;

    public DeliverySpeedDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.delivery_speed_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        ButterKnife.bind(this, dialogContainer);
        setRadioGroupDelivery();
    }

    private void setRadioGroupDelivery() {
        radioGroupDelivery.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId)
                {
                    case R.id.radio_btn_next_day:
                        mCallback.onDeliveryOptionsSelected(1);
                        break;
                    case R.id.radio_btn_express:
                        mCallback.onDeliveryOptionsSelected(2);
                        break;
                }
                isSelected = true;
            }
        });
    }

    @OnClick(R.id.btn_proceed) void onProceedClicked() {
        if (isSelected) {
            mCallback.onProccedClicked();
        } else {
            Toasty.warning(mContext, "Please Select Delivery Timing", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

    private int checkTimeSlot() {
        try {
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse("09:00:00");
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);


            Date time2 = new SimpleDateFormat("HH:mm:ss").parse("15:00:00");
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            Date time3 = new SimpleDateFormat("HH:mm:ss").parse("17:00:00");
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(time3);
            calendar3.add(Calendar.DATE, 1);

            Date time4 = new SimpleDateFormat("HH:mm:ss").parse(getTime());
            Calendar calendar4 = Calendar.getInstance();
            calendar4.setTime(time4);
            calendar4.add(Calendar.DATE, 1);

            Date currentTime = calendar4.getTime();

            if (currentTime.after(calendar1.getTime()) && currentTime.before(calendar2.getTime())) {
                return 1;
            } else if (currentTime.after(calendar2.getTime()) && currentTime.before(calendar3.getTime())) {
                return 2;
            } else {
                return 3;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setDeliveryExpress(double amount) {
        radioBtnExpress.setText("Express Delivery at Rs " + amount);
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
