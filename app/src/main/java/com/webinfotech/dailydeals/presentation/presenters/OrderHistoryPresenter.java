package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.presentation.presenters.base.BasePresenter;
import com.webinfotech.dailydeals.presentation.ui.adapters.OrdersAdapter;

/**
 * Created by Raj on 22-02-2019.
 */

public interface OrderHistoryPresenter extends BasePresenter {
    void getOrderHistory();
    interface View{
        void loadOrdersAdapter(OrdersAdapter adapter);
        void showProgressBar();
        void hideProgressBar();
    }
}
