package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.presentation.ui.adapters.StarProductsAdapter;

public interface StarProductsPresenter {
    void fetchStarProducts(int pageNo, String type);
    interface View {
        void loadData(StarProductsAdapter starProductsAdapter, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
