package com.webinfotech.dailydeals.presentation.ui.activities;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.dailydeals.R;
import com.webinfotech.dailydeals.domain.executors.impl.ThreadExecutor;
import com.webinfotech.dailydeals.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.dailydeals.domain.models.Testing.Brands;
import com.webinfotech.dailydeals.presentation.presenters.MainPresenter;
import com.webinfotech.dailydeals.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.dailydeals.presentation.ui.adapters.BrandsAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.dailydeals.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.dailydeals.threading.MainThreadImpl;
import com.webinfotech.dailydeals.util.GlideHelper;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.dots_layout) LinearLayout dotsLinearLayout;
    @BindView(R.id.recycler_view_categories) RecyclerView recyclerViewCategories;
    @BindView(R.id.recycler_view_popular_products) RecyclerView recyclerViewPopularProducts;
    @BindView(R.id.recycler_view_new_arrival) RecyclerView recyclerViewNewArrival;
    @BindView(R.id.recycler_view_best_selling) RecyclerView recyclerViewBestSelling;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    MainPresenterImpl mPresenter;
    private static int currentPage = 0;
    @BindView(R.id.main_layout)
    ConstraintLayout constraintLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.getCategories();
        mPresenter.getMainData();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsLinearLayout.getChildCount() > 0){
            dotsLinearLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(18, 18);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLinearLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadCategoriesAdapter(MainCategoriesAdapter adapter) {
        recyclerViewCategories.setAdapter(adapter);
        recyclerViewCategories.setLayoutManager(new GridLayoutManager(this, 3));
    }

    @Override
    public void goToSubcategoryActivity(int catId, String categoryName) {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("catId", catId);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void goToProductList(int categoryId, String categoryName) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", 1);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductListVerticalAdapter bestSellingProductsAdapter, ProductListVerticalAdapter popularProductsAdapter, ImageSlider[] imageSliders) {
        recyclerViewNewArrival.setAdapter(newProductsAdapter);
        recyclerViewNewArrival.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewPopularProducts.setAdapter(popularProductsAdapter);
        recyclerViewPopularProducts.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewBestSelling.setAdapter(bestSellingProductsAdapter);
        recyclerViewBestSelling.setLayoutManager(new GridLayoutManager(this, 2));
        HomeSlidingImagesAdapter homeSlidingImagesAdapter = new HomeSlidingImagesAdapter(this, imageSliders);
        viewPager.setAdapter(homeSlidingImagesAdapter);
        prepareDotsIndicator(0, imageSliders.length);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageSliders.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == imageSliders.length)
                {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int count) {
        if (count == 0) {
            txtViewCartCount.setVisibility(View.GONE);
        } else {
            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText(Integer.toString(count));
        }
    }

    @Override
    public void loadStarProductsCount(int count) {

    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showGoToCartSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"Added To Cart",Snackbar.LENGTH_LONG);
        snackbar.setAction("Go To Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartListActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
        mPresenter.fetchStarProductsCount();
    }
}
