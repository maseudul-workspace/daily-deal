package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.domain.models.Wallet.WalletDetails;
import com.webinfotech.dailydeals.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory(int page, String type);
    interface View {
        void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
