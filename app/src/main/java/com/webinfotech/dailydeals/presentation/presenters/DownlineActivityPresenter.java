package com.webinfotech.dailydeals.presentation.presenters;

import com.webinfotech.dailydeals.presentation.presenters.base.BasePresenter;
import com.webinfotech.dailydeals.presentation.ui.adapters.DownlinesAdapter;

/**
 * Created by Raj on 27-02-2019.
 */

public interface DownlineActivityPresenter extends BasePresenter {
    void getDownlineList();
    interface View{
        void showProgressBar();
        void hideProgressBar();
        void loadData(DownlinesAdapter adapter);
    }
}
