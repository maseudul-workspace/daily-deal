package com.webinfotech.dailydeals.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.GetSubcategoryListInteractor;
import com.webinfotech.dailydeals.domain.interactors.impl.GetSubcategoryListInteractorImpl;
import com.webinfotech.dailydeals.domain.models.Category.SubCategory;
import com.webinfotech.dailydeals.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.dailydeals.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.dailydeals.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.dailydeals.repository.Category.CategoryRepositoryImpl;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, GetSubcategoryListInteractor.Callback, SubcategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;
    GetSubcategoryListInteractorImpl getSubcategoryListInteractor;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int catId) {
        getSubcategoryListInteractor = new GetSubcategoryListInteractorImpl(mExecutor, mMainThread, new CategoryRepositoryImpl(), this, catId);
        getSubcategoryListInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(SubCategory[] subcategories) {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(mContext, subcategories, this);
        mView.loadSubcategoriesAdapter(subcategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId, String subcategoryName) {
        mView.goToProductList(subcategoryId, subcategoryName);
    }
}
