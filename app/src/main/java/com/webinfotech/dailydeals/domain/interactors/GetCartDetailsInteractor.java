package com.webinfotech.dailydeals.domain.interactors;


import com.webinfotech.dailydeals.domain.interactors.base.Interactor;
import com.webinfotech.dailydeals.domain.models.Cart.Cart;

/**
 * Created by Raj on 10-01-2019.
 */

public interface GetCartDetailsInteractor extends Interactor {
    interface Callback{
        void onGetCartDetailsSuccess(Cart[] carts);
        void onGetCartDetailsFail(String errorMsg);
    }
}
