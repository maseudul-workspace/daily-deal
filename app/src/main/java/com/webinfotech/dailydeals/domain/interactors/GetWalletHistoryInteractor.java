package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.Wallet.WalletDetails;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetWalletHistoryInteractor {

    interface Callback{
        void onGettingWalletHistorySuccess(WalletDetails walletDetails, int totalPage);
        void onGettingWalletHistoryFail(String errorMsg);
    }

}
