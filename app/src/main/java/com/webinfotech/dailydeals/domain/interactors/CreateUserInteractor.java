package com.webinfotech.dailydeals.domain.interactors;

public interface CreateUserInteractor {
    interface Callback {
        void onUserCreateSuccess();
        void onUserCreateFail(String errorMsg);
    }
}
