package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.User.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onAddressFetchSuccess(ShippingAddress[] shippingAddresses);
        void onAddressFetchFail(String errorMsg);
    }
}
