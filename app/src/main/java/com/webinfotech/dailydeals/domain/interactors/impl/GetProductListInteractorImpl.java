package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.GetProductListInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.Products.Product;
import com.webinfotech.dailydeals.domain.models.Products.ProductListWrapper;
import com.webinfotech.dailydeals.repository.ProductRepository.impl.GetProductsRepositoryImpl;

public class GetProductListInteractorImpl extends AbstractInteractor implements GetProductListInteractor {

    GetProductsRepositoryImpl mRepository;
    Callback mCallback;
    int type;
    int pageNo;
    int categoryId;

    public GetProductListInteractorImpl(Executor threadExecutor, MainThread mainThread, GetProductsRepositoryImpl mRepository, Callback mCallback, int type, int pageNo, int categoryId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.type = type;
        this.pageNo = pageNo;
        this.categoryId = categoryId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.getProductListByCategory(type, pageNo, categoryId);
        if (productListWrapper == null) {
            notifyError("Something Went Wrong");
        } else if(!productListWrapper.status) {
            notifyError(productListWrapper.message);
        } else {
            postMessage(productListWrapper.products, productListWrapper.totalPage);
        }
    }
}
