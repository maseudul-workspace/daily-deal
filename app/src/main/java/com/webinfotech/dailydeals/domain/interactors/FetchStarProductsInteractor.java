package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.Products.StarProduct;

public interface FetchStarProductsInteractor {
    interface Callback {
        void onGettingStarProductsSuccess(StarProduct[] starProducts, int totalPage);
        void onGettingStarProductsFail(String errorMsg);
    }
}
