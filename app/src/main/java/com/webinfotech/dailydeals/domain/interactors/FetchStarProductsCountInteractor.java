package com.webinfotech.dailydeals.domain.interactors;

public interface FetchStarProductsCountInteractor {
    interface Callback {
        void onGettingStarProductsCountSuccess(int starProductsCount);
        void onGettingStarProductsCountFail(String errorMsg);
    }
}
