package com.webinfotech.dailydeals.domain.interactors;

public interface FetchCouponInteractor {
    interface Callback {
        void onGettingCouponSuccess(double coupon);
        void onGettingCouponFail(String message);
    }
}
