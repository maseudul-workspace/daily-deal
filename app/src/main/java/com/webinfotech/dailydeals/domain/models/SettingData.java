package com.webinfotech.dailydeals.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingData {

    @SerializedName("about")
    @Expose
    public String about;

    @SerializedName("order_schedule")
    @Expose
    public String orderSchedule;

    @SerializedName("delivery_schedule")
    @Expose
    public String deliverySchedule;

    @SerializedName("contact_us")
    @Expose
    public String contactUs;

}
