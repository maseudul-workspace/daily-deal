package com.webinfotech.dailydeals.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargesWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("min_purchase_amount")
    @Expose
    public double minPurchaseAmount;

    @SerializedName("min_purchase_charge")
    @Expose
    public double minPurchaseCharge;

    @SerializedName("express_charge")
    @Expose
    public double expressCharge;

}
