package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.ChargesWrapper;

public interface GetChargesInteractor {
    interface Callback {
        void onGettingChargesSuccess(ChargesWrapper chargesWrapper);
        void onGettingChargesFail(String message);
    }
}
