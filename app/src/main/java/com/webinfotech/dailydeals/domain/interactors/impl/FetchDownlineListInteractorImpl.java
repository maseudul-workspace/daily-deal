package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.FetchDownlineInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.User.Downline;
import com.webinfotech.dailydeals.domain.models.User.DownlineWrapper;
import com.webinfotech.dailydeals.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 27-02-2019.
 */

public class FetchDownlineListInteractorImpl extends AbstractInteractor implements FetchDownlineInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public FetchDownlineListInteractorImpl(Executor threadExecutor,
                                           MainThread mainThread,
                                           Callback callback,
                                           UserRepositoryImpl repository,
                                           String apiKey,
                                           int userId
                                           ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDownlineListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Downline[] downlines){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDownlineListSuccess(downlines);
            }
        });
    }

    @Override
    public void run() {
        final DownlineWrapper downlineWrapper = mRepository.fetchDownlineList(apiKey, userId);
        if(downlineWrapper == null){
            notifyError("Something went wrong");
        }else if(!downlineWrapper.status){
            notifyError(downlineWrapper.message);
        }else{
            postMessage(downlineWrapper.downlines);
        }
    }
}
