package com.webinfotech.dailydeals.domain.models.Testing;

public class Subcategory {

    public String subcategoryName;
    public String subcategoryImage;

    public Subcategory(String subcategoryName, String subcategoryImage) {
        this.subcategoryName = subcategoryName;
        this.subcategoryImage = subcategoryImage;
    }
}
