package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.FetchCouponInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.CouponWrapper;
import com.webinfotech.dailydeals.domain.models.User.Downline;
import com.webinfotech.dailydeals.repository.ProductRepository.impl.CartRepositoryImpl;

public class FetchCouponInteractorImpl extends AbstractInteractor implements FetchCouponInteractor {

    CartRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String coupon;

    public FetchCouponInteractorImpl(Executor threadExecutor, MainThread mainThread, CartRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String coupon) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.coupon = coupon;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCouponFail(errorMsg);
            }
        });
    }

    private void postMessage(final double coupon){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCouponSuccess(coupon);
            }
        });
    }

    @Override
    public void run() {
        final CouponWrapper couponWrapper = mRepository.fetchCoupon(userId, apiToken, coupon);
        if (couponWrapper == null) {
            notifyError("Slow Internet Connection");
        } else if (!couponWrapper.status) {
            notifyError("Something Went Wrong");
        } else {
            postMessage(couponWrapper.couponDiscount);
        }
    }
}
