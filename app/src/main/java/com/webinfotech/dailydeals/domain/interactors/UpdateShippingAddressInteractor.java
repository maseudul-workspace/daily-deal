package com.webinfotech.dailydeals.domain.interactors;

public interface UpdateShippingAddressInteractor {
    interface Callback {
        void onEditAddressSuccess();
        void onEditAddressFail(String errorMsg);
    }
}
