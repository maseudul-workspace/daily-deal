package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.User.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
