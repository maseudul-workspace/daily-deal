package com.webinfotech.dailydeals.domain.models.Testing;

public class MainCategories {

    public String imageUrl;
    public String text;

    public MainCategories(String imageUrl, String text) {
        this.imageUrl = imageUrl;
        this.text = text;
    }

}
