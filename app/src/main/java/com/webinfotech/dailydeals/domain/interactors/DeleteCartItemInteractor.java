package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.interactors.base.Interactor;

/**
 * Created by Raj on 10-01-2019.
 */

public interface DeleteCartItemInteractor extends Interactor {
    interface Callback{
        void onCartItemDeletedSuccess(String successMsg);
        void onCartItemDeleteFail(String errorMsg);
    }
}
