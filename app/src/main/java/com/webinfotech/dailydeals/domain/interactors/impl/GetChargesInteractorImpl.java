package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.GetChargesInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.ChargesWrapper;
import com.webinfotech.dailydeals.domain.models.MainData;
import com.webinfotech.dailydeals.repository.ProductRepository.impl.CartRepositoryImpl;

public class GetChargesInteractorImpl extends AbstractInteractor implements GetChargesInteractor {

    CartRepositoryImpl mRepository;
    Callback mCallback;

    public GetChargesInteractorImpl(Executor threadExecutor, MainThread mainThread, CartRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingChargesFail(errorMsg);
            }
        });
    }

    private void postMessage(ChargesWrapper chargesWrapper) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingChargesSuccess(chargesWrapper);
            }
        });
    }

    @Override
    public void run() {
        final ChargesWrapper chargesWrapper = mRepository.getCharges();
        if (chargesWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!chargesWrapper.status) {
            notifyError(chargesWrapper.message);
        } else {
            postMessage(chargesWrapper);
        }
    }
}
