package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.Category.Category;

public interface GetCategoryListInteractor {
    interface Callback {
        void onGettingCategoryListSuccess(Category[] categories);
        void onGettingCategoryFail(String errorMsg);
    }
}
