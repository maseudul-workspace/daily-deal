package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.OrderPlaceInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.Orders.OrderPlacedResponse;
import com.webinfotech.dailydeals.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 26-02-2019.
 */

public class OrderPlaceInteractorImpl extends AbstractInteractor implements OrderPlaceInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;
    int walletStatus;
    int shippingAddressId;
    int deliveryStatus;
    String coupon;

    public OrderPlaceInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl repository,
                                    String apiKey,
                                    int userId,
                                    int walletStatus,
                                    int shippingAddressId,
                                    int deliveryStatus,
                                    String coupon
                                    ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.walletStatus = walletStatus;
        this.shippingAddressId = shippingAddressId;
        this.deliveryStatus = deliveryStatus;
        this.coupon = coupon;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlacedFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlacedSuccess();
            }
        });
    }


    @Override
    public void run() {
        final OrderPlacedResponse orderPlacedResponse = mRepository.placeOrder(apiKey, userId, walletStatus, shippingAddressId, deliveryStatus, coupon);
        if(orderPlacedResponse == null){
            notifyError("Something went wrong");
        }else if(!orderPlacedResponse.status){
            notifyError(orderPlacedResponse.message);
        }else{
            postMessage();
        }
    }
}
