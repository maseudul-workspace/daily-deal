package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public interface FetchDownlineInteractor {
    interface Callback{
        void onGettingDownlineListSuccess(Downline[] downlines);
        void onGettingDownlineListFail(String errorMsg);
    }
}
