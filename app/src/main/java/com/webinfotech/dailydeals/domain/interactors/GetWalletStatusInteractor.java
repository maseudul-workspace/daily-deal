package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.Wallet.WalletStatus;

/**
 * Created by Raj on 21-02-2019.
 */

public interface GetWalletStatusInteractor {
    interface Callback{
        void onGettingWalletStatusSuccess(WalletStatus walletStatus);
        void onGettingWalletStatusFail(String errorMsg);
    }
}
