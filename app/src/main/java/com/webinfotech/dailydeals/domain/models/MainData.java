package com.webinfotech.dailydeals.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.dailydeals.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.dailydeals.domain.models.Products.Product;

public class MainData {

    @SerializedName("sliders")
    @Expose
    public ImageSlider[] imageSliders;

    @SerializedName("new_arrivals")
    @Expose
    public Product[] newArrivals;

    @SerializedName("popular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("bestselling_products")
    @Expose
    public Product[] bestSellingProducts;

    @SerializedName("setting_data")
    @Expose
    public SettingData settingData;

}
