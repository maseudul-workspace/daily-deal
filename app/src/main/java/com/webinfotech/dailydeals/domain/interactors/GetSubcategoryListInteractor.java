package com.webinfotech.dailydeals.domain.interactors;


import com.webinfotech.dailydeals.domain.models.Category.SubCategory;

public interface GetSubcategoryListInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(SubCategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
