package com.webinfotech.dailydeals.domain.interactors.impl;

import com.webinfotech.dailydeals.domain.executors.Executor;
import com.webinfotech.dailydeals.domain.executors.MainThread;
import com.webinfotech.dailydeals.domain.interactors.ChangePasswordInteractor;
import com.webinfotech.dailydeals.domain.interactors.base.AbstractInteractor;
import com.webinfotech.dailydeals.domain.models.User.PasswordChangeResponse;
import com.webinfotech.dailydeals.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 14-02-2019.
 */

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String newPassword;
    String oldPassword;
    String apiKey;
    int userId;

    public ChangePasswordInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repository,
                                        String newPassword,
                                        String oldPassword,
                                        String apiKey,
                                        int userId
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangingPasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangingPasswordSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final PasswordChangeResponse passwordChangeResponse = mRepository.changePassword(newPassword, oldPassword, apiKey, userId);
        if(passwordChangeResponse == null){
            notifyError("Something went wrong");
        }else if(!passwordChangeResponse.status){
            notifyError(passwordChangeResponse.message);
        }else{
            postMessage(passwordChangeResponse.message);
        }
    }
}
