package com.webinfotech.dailydeals.domain.interactors;

import com.webinfotech.dailydeals.domain.models.MainData;

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFail(String errorMsg);
    }
}
