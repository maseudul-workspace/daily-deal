package com.webinfotech.dailydeals;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.dailydeals.domain.models.SettingData;
import com.webinfotech.dailydeals.domain.models.User.UserInfo;

public class AndroidApplication extends Application {

    UserInfo userInfo;
    SettingData settingData;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_SANTIREKHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context) {
        UserInfo user;
        if(userInfo != null) {
            user = userInfo;
        } else {
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_SANTIREKHA_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            } else {
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setSettingData(Context context, SettingData settingData) {
        this.settingData = settingData;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_SANTIREKHA_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(this.settingData != null) {
            editor.putString("SETTING DATA", new Gson().toJson(settingData));
        } else {
            editor.putString("SETTING DATA", "");
        }

        editor.commit();
    }

    public SettingData getSettingData(Context context) {
        SettingData settingData;
        if(this.settingData != null) {
            settingData = this.settingData;
        } else {
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_SANTIREKHA_APP_PREFERENCES), Context.MODE_PRIVATE);
            String settingDataJson = sharedPref.getString("SETTING DATA","");
            if(settingDataJson.isEmpty()){
                settingData = null;
            } else {
                try {
                    settingData = new Gson().fromJson(settingDataJson, SettingData.class);
                    this.settingData = settingData;
                }catch (Exception e){
                    settingData = null;
                }
            }
        }
        return settingData;
    }

}
